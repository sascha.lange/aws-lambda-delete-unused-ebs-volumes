# Delete unused EBS volumes, keep them if tag Do_not_delete is attached to
# volume
import boto3

def lambda_handler(event, context):
    ec2 = boto3.client('ec2')

    # Get list of regions
    regions = ec2.describe_regions().get('Regions',[] )

    # Iterate over regions
    for region in regions:
        print "Checking region %s " % region['RegionName']
        reg = region['RegionName']

        # Connect to region
        ec2 = boto3.resource('ec2',region_name=reg)

        for vol in ec2.volumes.all():

            # vol.state availabe is the state for unused volumes
            # So we delete them if no tag is attached
            if  vol.state=='available':
                if vol.tags is None:
                    vid=vol.id
                    v=ec2.Volume(vol.id)
                    v.delete()
                    print "Deleted " +vid
                    continue

                # Iterate over tags
                for tag in vol.tags:
                    if tag['Key'] == 'Name':
                        value=tag['Value']

                        # Don't delete a volume with tag
                        if value != 'Do_not_delete' and vol.state=='available':
                            vid=vol.id
                            v=ec2.Volume(vol.id)
                            v.delete()
                            print "Deleted " +vid
